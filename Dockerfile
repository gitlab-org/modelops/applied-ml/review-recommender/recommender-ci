FROM golang:1.16-alpine AS dev-build

# Add maintainer info
LABEL maintainer="Alexander Chueshev <achueshev@gitlab.com>"

# Add dependencies
RUN apk add --no-cache ca-certificates

# Add source files to the /app/ folder
WORKDIR /app
COPY . .

# Download dependencies
RUN go mod download

# Build an executable
RUN CGO_ENABLED=0 go build cmd/recommenderci.go

FROM alpine:3.13

COPY --from=dev-build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=dev-build /app/recommenderci /app/recommenderci

ARG RECOMMENDER_URL
ENV RECOMMENDER_URL=$RECOMMENDER_URL
ENV TMP_DIR=/tmp

# Command to run the executable
ENTRYPOINT ["/app/recommenderci"]
