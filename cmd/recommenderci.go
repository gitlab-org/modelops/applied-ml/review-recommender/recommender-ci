package main

import (
	"log"
	"os"

	"gitlab.com/gitlab-org/modelops/applied-ml/review-recommender/ci/internal/cli"
)

func main() {
	app := cli.NewRecommenderCliApps()
	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
