module gitlab.com/gitlab-org/modelops/applied-ml/review-recommender/ci

go 1.16

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.2 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.1
	github.com/urfave/cli/v2 v2.5.0
	golang.org/x/sys v0.0.0-20220422013727-9388b58f7150 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
