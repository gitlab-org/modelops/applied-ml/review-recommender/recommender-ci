package cli

import (
	"time"

	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/modelops/applied-ml/review-recommender/ci/internal/domain"
)

const (
	name        = "recommender-cli"
	description = "Get Reviewer-Recommender recommendations by running GitLab CI jobs"

	/* Global flags */
	projectIdFlag        = "project-id"
	projectNameFlag      = "project-name"
	projectNamespaceFlag = "project-namespace"
	timeoutFlag          = "timeout"

	/* Auth flags */
	selectedAuthFlowFlag = "auth-flow"
	ciJobTokenFlag       = "ci-job-token"

	/* Reviewer-Recommender flags */
	recommenderUrlFlag  = "recommender-url"
	recommenderTopNFlag = "recommender-top-n"
	reportFlag          = "report"
	withPostFlag        = "with-post"

	/* Merge request identification by iid flags */
	mergeRequestIidFlag = "iid"

	/* Merge request identification by commit flags */
	mergeRequestCommitFlag        = "sha"
	mergeRequestRequiredStateFlag = "required-state"
)

const (
	envCiJobToken        = "CI_JOB_TOKEN"
	envCiCommitSha       = "CI_COMMIT_SHA"
	envCiMergeRequestIid = "CI_MERGE_REQUEST_IID"
	envRecommenderUrl    = "RECOMMENDER_URL"
	envRecommenderTopN   = "RECOMMENDER_TOP_N"
)

const (
	ciAuthFlow string = "ci-auth-flow"
)

const (
	metadataMergeRequestIdentity = "merge-request-identity"
)

const (
	defaultRequestTimeout  = 15 * time.Second
	defaultRecommenderTopN = 3
)

var (
	/* Global flags */
	projectIdFlagCli = &cli.Int64Flag{
		Name:     projectIdFlag,
		Required: true,
	}
	projectNameFlagCli = &cli.StringFlag{
		Name:     projectNameFlag,
		Required: true,
	}
	projectNamespaceFlagCli = &cli.StringFlag{
		Name:     projectNamespaceFlag,
		Required: true,
	}
	timeoutFlagCli = &cli.DurationFlag{
		Name:  timeoutFlag,
		Value: defaultRequestTimeout,
	}

	/* Auth flags */
	selectedAuthFlowFlagCli = &cli.StringFlag{
		Name:   selectedAuthFlowFlag,
		Hidden: true,
	}
	ciJobTokenFlagCli = &cli.StringFlag{
		Name:    ciJobTokenFlag,
		EnvVars: []string{envCiJobToken},
		Hidden:  true,
	}

	/* Reviewer Recommender flags */
	recommenderUrlFlagCli = &cli.StringFlag{
		Name:    recommenderUrlFlag,
		EnvVars: []string{envRecommenderUrl},
		Hidden:  true,
	}
	recommenderTopNFlagCli = &cli.IntFlag{
		Name:    recommenderTopNFlag,
		EnvVars: []string{envRecommenderTopN},
		Hidden:  true,
		Value:   defaultRecommenderTopN,
	}
	reportFlagCli = &cli.StringFlag{
		Name:     reportFlag,
		Required: true,
	}
	withPostFlagCli = &cli.BoolFlag{
		Name: withPostFlag,
	}

	/* Merge request flags */
	mergeRequestIidFlagCli = &cli.Int64Flag{
		Name:    mergeRequestIidFlag,
		EnvVars: []string{envCiMergeRequestIid},
		Hidden:  true,
	}

	/* Merge request by commit flags */
	mergeRequestCommitFlagCli = &cli.StringFlag{
		Name:    mergeRequestCommitFlag,
		EnvVars: []string{envCiCommitSha},
		Hidden:  true,
	}
	mergeRequestRequiredStateFlagCli = &cli.StringFlag{
		Name:  mergeRequestRequiredStateFlag,
		Value: "opened",
	}
)

func newCiAuthCommand() *cli.Command {
	return &cli.Command{
		Name:  "ci-auth",
		Usage: "Get recommendations running a CI job",
		Flags: []cli.Flag{
			selectedAuthFlowFlagCli,
			ciJobTokenFlagCli,
		},
		Before: func(clictx *cli.Context) (err error) {
			_ = clictx.Set(selectedAuthFlowFlag, ciAuthFlow)

			if !clictx.IsSet(ciJobTokenFlag) {
				err = ErrEnvRequired{Env: envCiJobToken}
			}
			return
		},
		Subcommands: []*cli.Command{
			newMergeRequestIdentityCommand(),
		},
	}
}

func newMergeRequestIdentityCommand() *cli.Command {
	return &cli.Command{
		Name:  "merge-request-identity",
		Usage: "Identify merge request either by iid or commit and state",
		Flags: []cli.Flag{
			mergeRequestIidFlagCli,
			mergeRequestCommitFlagCli,
			mergeRequestRequiredStateFlagCli,
		},
		Before: func(clictx *cli.Context) error {
			var identity domain.MergeRequestIdentity

			if clictx.IsSet(mergeRequestIidFlag) {
				identity = domain.MergeRequestByIid{
					Iid: clictx.Int64(mergeRequestIidFlag),
				}
			} else if clictx.IsSet(mergeRequestCommitFlag) {
				identity = domain.MergeRequestByCommit{
					Sha:   clictx.String(mergeRequestCommitFlag),
					State: clictx.String(mergeRequestRequiredStateFlag),
				}
			}

			if identity == nil {
				return domain.ErrMergeRequestIdentityNotFound
			}

			clictx.App.Metadata[metadataMergeRequestIdentity] = identity
			return nil
		},
		Subcommands: []*cli.Command{
			newRecommendReviewersCommand(),
		},
	}
}

func newRecommendReviewersCommand() *cli.Command {
	return &cli.Command{
		Name:    "recommend-reviewers",
		Aliases: []string{"rr"},
		Usage:   "Recommend code reviewers for the changed files",
		Flags: []cli.Flag{
			recommenderUrlFlagCli,
			recommenderTopNFlagCli,
			reportFlagCli,
			withPostFlagCli,
		},
		Before: func(ctx *cli.Context) (err error) {
			if !ctx.IsSet(recommenderUrlFlag) {
				err = ErrEnvRequired{Env: envRecommenderUrl}
			}
			return
		},
		Action: recommendMergeRequestReviewersAction,
	}
}

func NewRecommenderCliApps() *cli.App {
	return &cli.App{
		Name:  name,
		Usage: description,
		Flags: []cli.Flag{
			projectIdFlagCli,
			projectNameFlagCli,
			projectNamespaceFlagCli,
			timeoutFlagCli,
		},
		Commands: []*cli.Command{
			newCiAuthCommand(),
		},
	}
}
