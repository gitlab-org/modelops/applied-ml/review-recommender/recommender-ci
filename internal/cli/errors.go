package cli

import "fmt"

type (
	ErrEnvRequired struct {
		Env string
	}
)

func (e ErrEnvRequired) Error() string {
	return fmt.Sprintf("env. variable '%s' is required", e.Env)
}
