package cli

import (
	"context"
	"net/http"

	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/modelops/applied-ml/review-recommender/ci/internal/domain"
	"gitlab.com/gitlab-org/modelops/applied-ml/review-recommender/ci/internal/recommender"
	"gitlab.com/gitlab-org/modelops/applied-ml/review-recommender/ci/internal/recommender/rr"
	jsonreporter "gitlab.com/gitlab-org/modelops/applied-ml/review-recommender/ci/internal/reporter/json"
)

func recommendMergeRequestReviewersAction(clictx *cli.Context) error {
	reporter := jsonreporter.New()
	rRecommender, err := rr.New(http.DefaultClient, clictx.String(recommenderUrlFlag))
	if err != nil {
		return err
	}

	projectId := clictx.Int64(projectIdFlag)
	identity := getMergeRequestIdentity(clictx)
	authConfig := getRecommenderAuthConfig(clictx)
	modelConfig := getRecommenderModelConfig(clictx)
	config := domain.RecommenderServiceConfig{Auth: authConfig, Model: modelConfig}

	ctx, cancel := context.WithTimeout(context.Background(), clictx.Duration(timeoutFlag))
	defer cancel()

	ucase := recommender.NewUseCase(rRecommender, reporter)
	rec, err := ucase.MergeRequestRecommendations(ctx, projectId, identity, config)
	if err != nil {
		return err
	}

	err = ucase.ReportMergeRequestRecommendations(ctx, rec, clictx.String(reportFlag))
	if clictx.Bool(withPostFlag) {
		err = ucase.PostMergeRequestRecommendations(ctx, rec, authConfig)
	}

	return err
}

func getMergeRequestIdentity(clictx *cli.Context) domain.MergeRequestIdentity {
	return clictx.App.Metadata[metadataMergeRequestIdentity].(domain.MergeRequestIdentity)
}

func getRecommenderAuthConfig(clictx *cli.Context) (config domain.RecommenderAuthConfig) {
	switch clictx.String(selectedAuthFlowFlag) {
	case ciAuthFlow:
		config = domain.RecommenderAuthConfig{JobToken: clictx.String(ciJobTokenFlag)}
	}
	return
}

func getRecommenderModelConfig(clictx *cli.Context) domain.RecommenderModelConfig {
	topN := clictx.Int(recommenderTopNFlag)
	return domain.RecommenderModelConfig{
		ProjectName:      clictx.String(projectNameFlag),
		ProjectNamespace: clictx.String(projectNamespaceFlag),
		TopN:             int32(topN),
	}
}
