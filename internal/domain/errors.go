package domain

import "errors"

var (
	ErrMergeRequestIdentityNotFound = errors.New("merge request identification strategy was not found")
)
