package domain

import (
	"context"
)

type (
	RecommenderAuthConfig struct {
		JobToken string
	}
	RecommenderModelConfig struct {
		ProjectName      string
		ProjectNamespace string
		TopN             int32
	}
	RecommenderServiceConfig struct {
		Auth  RecommenderAuthConfig
		Model RecommenderModelConfig
	}
)

type (
	MergeRequestIdentity interface {
		isMergeRequest()
	}
	MergeRequestByIid struct {
		Iid int64
	}
	MergeRequestByCommit struct {
		Sha   string
		State string
	}
)

type (
	Recommendations struct {
		TopN      int32    `json:"topN"`
		Version   string   `json:"version"`
		Iid       int64    `json:"iid,string"`
		ProjectId int64    `json:"projectId,string"`
		Changes   []string `json:"changes"`
		Reviewers []string `json:"reviewers"`
	}
)

type Recommender interface {
	RecommendationsByMergeRequestIid(
		ctx context.Context, projectId, iid int64, config RecommenderServiceConfig) (
		Recommendations, error,
	)
	RecommendationsByMergeRequestCommit(
		ctx context.Context, projectId int64, sha, state string, config RecommenderServiceConfig) (
		Recommendations, error,
	)
	PostMergeRequestRecommendations(
		ctx context.Context, recommendations Recommendations, config RecommenderAuthConfig) error
}

type RecommenderUseCase interface {
	MergeRequestRecommendations(
		ctx context.Context, projectId int64, identity MergeRequestIdentity, config RecommenderServiceConfig) (
		Recommendations, error,
	)
	PostMergeRequestRecommendations(
		ctx context.Context, recommendations Recommendations, config RecommenderAuthConfig) error
	ReportMergeRequestRecommendations(
		ctx context.Context, recommendations Recommendations, filename string) error
}

func (_ MergeRequestByIid) isMergeRequest() {}

func (_ MergeRequestByCommit) isMergeRequest() {}
