package domain

import "context"

type (
	ReportMeta struct {
		TopN    int32  `json:"top_n"`
		Version string `json:"version"`
	}

	ReportMergeRequest struct {
		ProjectId    int64    `json:"project_id"`
		Iid          int64    `json:"iid"`
		ChangedFiles []string `json:"changed_files"`
	}

	ReportRecommendations struct {
		Reviewers []string `json:"reviewers"`
	}

	Report struct {
		Meta            ReportMeta            `json:"meta"`
		MergeRequest    ReportMergeRequest    `json:"merge_request"`
		Recommendations ReportRecommendations `json:"recommendations"`
	}
)

type Reporter interface {
	Write(ctx context.Context, filename string, report Report) error
}
