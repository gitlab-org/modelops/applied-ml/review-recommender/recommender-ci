package gitutils

import (
	"bytes"
	"fmt"
	"os/exec"
	"strings"
)

func FetchRef(project, remote string, ref string) (err error) {
	cmd := exec.Command(
		"git",
		"-C",
		project,
		"fetch",
		remote,
		fmt.Sprintf("%s:%s", ref, ref),
	)

	var stderr bytes.Buffer
	cmd.Stderr = &stderr
	if err = cmd.Run(); err != nil {
		err = fmt.Errorf("%s", stderr.String())
	}

	return
}

func ChangedFiles(project, baseSha, headSha string) ([]string, error) {
	cmd := exec.Command(
		"git",
		"-C",
		project,
		"diff",
		baseSha,
		headSha,
		"--name-only",
	)

	var stdout bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	if err := cmd.Run(); err != nil {
		return nil, fmt.Errorf("%s", stderr.String())
	}

	stdout.Truncate(stdout.Len() - 1) // remove the newline character ('\n') at the end
	changedFiles := strings.Split(stdout.String(), "\n")

	return changedFiles, nil
}
