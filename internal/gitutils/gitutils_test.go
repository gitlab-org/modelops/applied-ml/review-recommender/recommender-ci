package gitutils

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"reflect"
	"sort"
	"testing"

	log "github.com/sirupsen/logrus"
)

const (
	testPhpRepositoryUrl = "https://gitlab.com/gitlab-examples/php.git"
)

type testRepository struct {
	Url    string
	Remote string
	Dir    string
}

type testMergeRequest struct {
	Number       int
	BaseShaDiff  string
	HeadShaDiff  string
	ChangedFiles []string
}

func (r *testRepository) CloneRepository(url string) error {
	tmpDir, err := ioutil.TempDir("", "gitutils-*")
	if err != nil {
		return err
	}

	cmd := exec.Command("git", "clone", url, tmpDir)
	if err = cmd.Run(); err != nil {
		return err
	}

	r.Url = url
	r.Remote = "origin"
	r.Dir = tmpDir

	return nil
}

func (r *testRepository) DeleteRepository() {
	if err := os.RemoveAll(r.Dir); err != nil {
		log.Warning(err)
	}

	r.Dir = ""
	r.Remote = ""
}

func (mr *testMergeRequest) Name() string {
	return fmt.Sprintf("MR#%d", mr.Number)
}

func (mr *testMergeRequest) Ref() string {
	return fmt.Sprintf("refs/merge-requests/%d/head", mr.Number)
}

var testRepo = new(testRepository)

var testMergeRequests = []testMergeRequest{
	{
		Number:       7,
		BaseShaDiff:  "260544753075860b5d511651f8d69c61f9a1b8ff",
		HeadShaDiff:  "0e8237bc3efe99eaac95768a119f40d612906b9d",
		ChangedFiles: []string{"README.md"},
	},
	{
		Number:       37,
		BaseShaDiff:  "d079b083e1e5cd8e648bfd33b800859aa78088df",
		HeadShaDiff:  "d69fff3e7da4f52b6557edf9fa20583ac20025bc",
		ChangedFiles: []string{"Tests/HelloWorldTest.php", ".gitlab-ci.yml", "composer.json"},
	},
	{
		Number:       39,
		BaseShaDiff:  "d079b083e1e5cd8e648bfd33b800859aa78088df",
		HeadShaDiff:  "a5c991ad804bb05563aedd2d6339c7a2af822983",
		ChangedFiles: []string{".gitlab-ci.yml", "composer.json"},
	},
}

func TestMain(m *testing.M) {
	var exitCode int
	if err := testRepo.CloneRepository(testPhpRepositoryUrl); err == nil {
		exitCode = m.Run()
		testRepo.DeleteRepository()
	} else {
		log.Error(err)
	}

	os.Exit(exitCode)
}

func TestFetchRefs(t *testing.T) {
	for _, mergeRequest := range testMergeRequests {
		t.Run(mergeRequest.Name(), func(t *testing.T) {
			if err := FetchRef(testRepo.Dir, testRepo.Remote, mergeRequest.Ref()); err != nil {
				t.Error(err)
			}

			cmd := exec.Command(
				"git",
				"-C",
				testRepo.Dir,
				"show-ref",
				"--verify",
				mergeRequest.Ref(),
			)
			var stderr bytes.Buffer
			cmd.Stderr = &stderr
			if err := cmd.Run(); err != nil {
				t.Error(stderr.String())
			}
		})
	}
}

func TestChangedFiles(t *testing.T) {
	for _, mergeRequest := range testMergeRequests {
		t.Run(mergeRequest.Name(), func(t *testing.T) {
			changedFiles, err := ChangedFiles(testRepo.Dir, mergeRequest.BaseShaDiff, mergeRequest.HeadShaDiff)
			if err != nil {
				t.Error(err)
			}

			sort.Strings(changedFiles)
			sort.Strings(mergeRequest.ChangedFiles)

			if !reflect.DeepEqual(changedFiles, mergeRequest.ChangedFiles) {
				t.Errorf("got %v but expected %v", changedFiles, mergeRequest.ChangedFiles)
			}
		})
	}
}
