package rr

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"path"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/modelops/applied-ml/review-recommender/ci/internal/domain"
)

const (
	headerJobToken  = "Job-Token"
	headerProjectId = "Project-Id"
)

type errBadStatus struct {
	Code int
}

func (e errBadStatus) Error() string {
	return fmt.Sprintf("reviewer-recommender server returned code %d", e.Code)
}

type (
	recommendationsByMrIidEndpoint struct {
		baseUrl   *url.URL
		projectId int64
		iid       int64
		config    domain.RecommenderModelConfig
	}
	recommendationsByMrCommitEndpoint struct {
		baseUrl   *url.URL
		projectId int64
		sha       string
		state     string
		config    domain.RecommenderModelConfig
	}
	postMrRecommendationsEndpoint struct {
		baseUrl   *url.URL
		projectId int64
		iid       int64
	}
)

type recommender struct {
	baseUrl    *url.URL
	httpClient *http.Client
}

func (e recommendationsByMrIidEndpoint) String() string {
	endpoint := *e.baseUrl
	endpoint.Path = path.Join(
		e.baseUrl.Path, "projects",
		fmt.Sprintf("%d", e.projectId),
		"merge_requests",
		fmt.Sprintf("%d", e.iid),
		"recommendations", "reviewers",
	)

	query := endpoint.Query()
	query.Set("topN", fmt.Sprintf("%d", e.config.TopN))
	query.Set("projectName", e.config.ProjectName)
	query.Set("projectNamespace", e.config.ProjectNamespace)
	endpoint.RawQuery = query.Encode()

	return endpoint.String()
}

func (e recommendationsByMrCommitEndpoint) String() string {
	endpoint := *e.baseUrl
	endpoint.Path = path.Join(
		e.baseUrl.Path, "projects",
		fmt.Sprintf("%d", e.projectId),
		"commits", e.sha,
		"merge_request", "recommendations", "reviewers",
	)

	query := endpoint.Query()
	query.Set("topN", fmt.Sprintf("%d", e.config.TopN))
	query.Set("projectName", e.config.ProjectName)
	query.Set("projectNamespace", e.config.ProjectNamespace)
	query.Set("mergeRequestByCommit.state", e.state)
	endpoint.RawQuery = query.Encode()

	return endpoint.String()
}

func (e postMrRecommendationsEndpoint) String() string {
	endpoint := *e.baseUrl
	endpoint.Path = path.Join(
		e.baseUrl.Path, "projects",
		fmt.Sprintf("%d", e.projectId),
		"merge_requests",
		fmt.Sprintf("%d", e.iid),
		"recommendations", "reviewers", "notes",
	)

	return endpoint.String()
}

func (r *recommender) RecommendationsByMergeRequestIid(
	ctx context.Context, projectId, iid int64, config domain.RecommenderServiceConfig) (
	domain.Recommendations, error,
) {
	var recommendations domain.Recommendations
	endpoint := recommendationsByMrIidEndpoint{
		baseUrl:   r.baseUrl,
		projectId: projectId,
		iid:       iid,
		config:    config.Model,
	}
	request, err := http.NewRequest("GET", endpoint.String(), nil)
	if err != nil {
		return recommendations, err
	}

	response, err := r.doRequest(ctx, request, projectId, config.Auth)
	if err != nil {
		return recommendations, err
	}

	defer func() {
		if err := response.Body.Close(); err != nil {
			log.Warning(err)
		}
	}()

	err = json.NewDecoder(response.Body).Decode(&recommendations)
	if err != nil {
		return recommendations, err
	}

	return recommendations, nil
}

func (r *recommender) RecommendationsByMergeRequestCommit(
	ctx context.Context, projectId int64, sha string, state string, config domain.RecommenderServiceConfig,
) (
	domain.Recommendations, error,
) {
	var recommendations domain.Recommendations
	endpoint := recommendationsByMrCommitEndpoint{
		baseUrl:   r.baseUrl,
		projectId: projectId,
		sha:       sha,
		state:     state,
		config:    config.Model,
	}
	request, err := http.NewRequest("GET", endpoint.String(), nil)
	if err != nil {
		return recommendations, err
	}

	response, err := r.doRequest(ctx, request, projectId, config.Auth)
	if err != nil {
		return recommendations, err
	}

	defer func() {
		if err := response.Body.Close(); err != nil {
			log.Warning(err)
		}
	}()

	err = json.NewDecoder(response.Body).Decode(&recommendations)
	if err != nil {
		return recommendations, err
	}

	return recommendations, nil
}

func (r *recommender) PostMergeRequestRecommendations(
	ctx context.Context, recommendations domain.Recommendations, config domain.RecommenderAuthConfig,
) error {
	endpoint := postMrRecommendationsEndpoint{
		baseUrl:   r.baseUrl,
		projectId: recommendations.ProjectId,
		iid:       recommendations.Iid,
	}

	payload := struct {
		Changes   []string `json:"changes"`
		Reviewers []string `json:"reviewers"`
	}{
		Changes:   recommendations.Changes,
		Reviewers: recommendations.Reviewers,
	}
	var payloadBuffer bytes.Buffer
	if err := json.NewEncoder(&payloadBuffer).Encode(payload); err != nil {
		return err
	}

	request, err := http.NewRequest("POST", endpoint.String(), &payloadBuffer)
	if err != nil {
		return err
	}

	response, err := r.doRequest(ctx, request, recommendations.ProjectId, config)
	if err != nil {
		return err
	}

	defer func() {
		if err := response.Body.Close(); err != nil {
			log.Warning(err)
		}
	}()

	return nil
}

func (r *recommender) doRequest(
	ctx context.Context, request *http.Request, projectId int64, auth domain.RecommenderAuthConfig) (
	*http.Response, error,
) {
	if ctx == nil {
		ctx = context.Background()
	}

	request = request.WithContext(ctx)
	request = setHeaders(request, map[string]string{
		"Content-Type":  "application/json",
		headerJobToken:  auth.JobToken,
		headerProjectId: fmt.Sprintf("%d", projectId),
	})

	response, err := r.httpClient.Do(request)
	if err != nil {
		return nil, err
	}

	if !(response.StatusCode >= 200 && response.StatusCode < 300) {
		return nil, errBadStatus{response.StatusCode}
	}

	return response, nil
}

func setHeaders(request *http.Request, headers map[string]string) *http.Request {
	for h, v := range headers {
		request.Header.Set(h, v)
	}
	return request
}

func New(httpClient *http.Client, baseUrl string) (*recommender, error) {
	baseUrlParsed, err := url.Parse(baseUrl)
	if err != nil {
		return nil, err
	}

	return &recommender{
		baseUrl:    baseUrlParsed,
		httpClient: httpClient,
	}, nil
}
