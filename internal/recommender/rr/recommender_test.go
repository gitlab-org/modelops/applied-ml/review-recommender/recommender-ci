package rr

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/modelops/applied-ml/review-recommender/ci/internal/domain"
)

// Unit tests

var (
	testExpectedRecommendations = domain.Recommendations{
		TopN:      3,
		Version:   "v0.1.0",
		Iid:       1,
		ProjectId: 995,
		Changes:   []string{"fileA"},
		Reviewers: []string{"reviewer1", "reviewer2", "reviewer3"},
	}
)

func TestRecommender_RecommendationsByMergeRequestIid(t *testing.T) {
	mux := http.NewServeMux()
	endpoint := fmt.Sprintf("/projects/%d/merge_requests/%d/recommendations/reviewers",
		testExpectedRecommendations.ProjectId, testExpectedRecommendations.Iid,
	)
	mux.HandleFunc(endpoint, func(w http.ResponseWriter, req *http.Request) {
		hJobToken := req.Header.Get(headerJobToken)
		hProjectId := req.Header.Get(headerProjectId)
		if hJobToken == "" || hProjectId == "" {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		if err := json.NewEncoder(w).Encode(testExpectedRecommendations); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	})

	s := httptest.NewServer(mux)
	defer s.Close()

	tests := []struct {
		TestName                string
		ProjectIid              int64
		Iid                     int64
		Config                  domain.RecommenderServiceConfig
		ExpectedRecommendations domain.Recommendations
		ExpectedError           error
	}{
		{
			TestName:   "found",
			ProjectIid: testExpectedRecommendations.ProjectId,
			Iid:        testExpectedRecommendations.Iid,
			Config: domain.RecommenderServiceConfig{
				Auth:  domain.RecommenderAuthConfig{JobToken: "access-token"},
				Model: domain.RecommenderModelConfig{ProjectName: "project", TopN: testExpectedRecommendations.TopN},
			},
			ExpectedRecommendations: testExpectedRecommendations,
		},
		{
			TestName:   "not-found",
			ProjectIid: 995,
			Iid:        0,
			Config: domain.RecommenderServiceConfig{
				Auth:  domain.RecommenderAuthConfig{JobToken: "access-token"},
				Model: domain.RecommenderModelConfig{ProjectName: "project", TopN: testExpectedRecommendations.TopN},
			},
			ExpectedError: errBadStatus{Code: http.StatusNotFound},
		},
		{
			TestName:   "unauthorized",
			ProjectIid: testExpectedRecommendations.ProjectId,
			Iid:        testExpectedRecommendations.Iid,
			Config: domain.RecommenderServiceConfig{
				Model: domain.RecommenderModelConfig{ProjectName: "project", TopN: testExpectedRecommendations.TopN},
			},
			ExpectedError: errBadStatus{Code: http.StatusUnauthorized},
		},
	}

	for _, test := range tests {
		t.Run(test.TestName, func(t *testing.T) {
			r, err := New(s.Client(), s.URL)
			require.NoError(t, err)

			actual, err := r.RecommendationsByMergeRequestIid(context.TODO(), test.ProjectIid, test.Iid, test.Config)
			assert.ErrorIs(t, err, test.ExpectedError)
			assert.Equal(t, test.ExpectedRecommendations, actual)
		})
	}
}

func TestRecommender_RecommendationsByMergeRequestCommit(t *testing.T) {
	expectedSha := "abcde"
	expectedState := "opened"

	mux := http.NewServeMux()
	endpoint := fmt.Sprintf("/projects/%d/commits/%s/merge_request/recommendations/reviewers",
		testExpectedRecommendations.ProjectId, expectedSha,
	)
	mux.HandleFunc(endpoint, func(w http.ResponseWriter, req *http.Request) {
		hJobToken := req.Header.Get(headerJobToken)
		hProjectId := req.Header.Get(headerProjectId)
		if hJobToken == "" || hProjectId == "" {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		if req.URL.Query().Get("mergeRequestByCommit.state") != expectedState {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		if err := json.NewEncoder(w).Encode(testExpectedRecommendations); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	})

	s := httptest.NewServer(mux)
	defer s.Close()

	tests := []struct {
		TestName                string
		ProjectId               int64
		Sha                     string
		State                   string
		Config                  domain.RecommenderServiceConfig
		ExpectedRecommendations domain.Recommendations
		ExpectedError           error
	}{
		{
			TestName:  "found",
			ProjectId: testExpectedRecommendations.ProjectId,
			Sha:       expectedSha,
			State:     expectedState,
			Config: domain.RecommenderServiceConfig{
				Auth:  domain.RecommenderAuthConfig{JobToken: "access-token"},
				Model: domain.RecommenderModelConfig{ProjectName: "project", TopN: testExpectedRecommendations.TopN},
			},
			ExpectedRecommendations: testExpectedRecommendations,
		},
		{
			TestName:  "not-found-by-sha",
			ProjectId: testExpectedRecommendations.ProjectId,
			Sha:       "xyzabc",
			State:     expectedState,
			Config: domain.RecommenderServiceConfig{
				Auth: domain.RecommenderAuthConfig{JobToken: "access-token"},
			},
			ExpectedError: errBadStatus{Code: http.StatusNotFound},
		},
		{
			TestName:  "not-found-by-state",
			ProjectId: testExpectedRecommendations.ProjectId,
			Sha:       expectedSha,
			State:     "closed",
			Config: domain.RecommenderServiceConfig{
				Auth: domain.RecommenderAuthConfig{JobToken: "access-token"},
			},
			ExpectedError: errBadStatus{Code: http.StatusNotFound},
		},
		{
			TestName:      "unauthorized",
			ProjectId:     testExpectedRecommendations.ProjectId,
			Sha:           expectedSha,
			ExpectedError: errBadStatus{Code: http.StatusUnauthorized},
		},
	}

	for _, test := range tests {
		t.Run(test.TestName, func(t *testing.T) {
			r, err := New(s.Client(), s.URL)
			require.NoError(t, err)

			actual, err := r.RecommendationsByMergeRequestCommit(
				context.TODO(), test.ProjectId, test.Sha, test.State, test.Config,
			)
			assert.ErrorIs(t, err, test.ExpectedError)
			assert.Equal(t, test.ExpectedRecommendations, actual)
		})
	}
}

func TestRecommender_PostMergeRequestRecommendations(t *testing.T) {
	mux := http.NewServeMux()
	endpoint := fmt.Sprintf(
		"/projects/%d/merge_requests/%d/recommendations/reviewers/notes",
		testExpectedRecommendations.ProjectId, testExpectedRecommendations.Iid,
	)
	mux.HandleFunc(endpoint, func(w http.ResponseWriter, req *http.Request) {
		hJobToken := req.Header.Get(headerJobToken)
		hProjectId := req.Header.Get(headerProjectId)
		if hJobToken == "" || hProjectId == "" {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		if req.Method != "POST" {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		if req.Header.Get("Content-Type") != "application/json" {
			w.WriteHeader(http.StatusUnsupportedMediaType)
			return
		}

		var message struct {
			Changes   []string `json:"changes"`
			Reviewers []string `json:"reviewers"`
		}
		if err := json.NewDecoder(req.Body).Decode(&message); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		w.WriteHeader(http.StatusCreated)
	})

	s := httptest.NewServer(mux)
	defer s.Close()

	tests := []struct {
		TestName        string
		Config          domain.RecommenderAuthConfig
		Recommendations domain.Recommendations
		ExpectedError   error
	}{
		{
			TestName:        "found",
			Config:          domain.RecommenderAuthConfig{JobToken: "access-token"},
			Recommendations: testExpectedRecommendations,
		},
		{
			TestName:      "not-found",
			Config:        domain.RecommenderAuthConfig{JobToken: "access-token"},
			ExpectedError: errBadStatus{Code: http.StatusNotFound},
		},
		{
			TestName:        "unauthorized",
			Recommendations: testExpectedRecommendations,
			ExpectedError:   errBadStatus{Code: http.StatusUnauthorized},
		},
	}

	for _, test := range tests {
		t.Run(test.TestName, func(t *testing.T) {
			r, err := New(s.Client(), s.URL)
			require.NoError(t, err)

			err = r.PostMergeRequestRecommendations(context.TODO(), test.Recommendations, test.Config)
			require.ErrorIs(t, err, test.ExpectedError)
		})
	}
}

// Integration tests

const (
	testEnvRecommenderBotUrl = "TEST_RECOMMENDER_BOT_URL"
)

func testIsIntegrationTestRunnable(envs ...string) (map[string]string, bool) {
	if testing.Short() {
		return nil, false
	}

	values := make(map[string]string)
	for _, env := range envs {
		if value, ok := os.LookupEnv(env); ok {
			values[env] = value
			continue
		}

		log.Warningf("env variable '%s' not set", env)
		return nil, false
	}

	return values, true
}

func TestRecommender_RecommendationsByMergeRequestIid_Integration(t *testing.T) {
	var ok bool
	var envs map[string]string
	if envs, ok = testIsIntegrationTestRunnable(testEnvRecommenderBotUrl); !ok {
		t.Skip("skipping this integration test")
	}

	// Use the Gitaly fork project to run this integration test
	// URL: https://gitlab.com/gitlab-org/modelops/applied-ml/review-recommender/gitaly
	var iid int64 = 3
	var projectId int64 = 31393306
	var project = "gitaly"
	var namespace = "gitlab-org/modelops/applied-ml/review-recommender"
	var topN int32 = 3

	baseUrl := envs[testEnvRecommenderBotUrl]
	r, err := New(http.DefaultClient, baseUrl)
	require.NoError(t, err)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	// use the testing service that doesn't require authentication
	recommendations, err := r.RecommendationsByMergeRequestIid(ctx, projectId, iid, domain.RecommenderServiceConfig{
		Model: domain.RecommenderModelConfig{ProjectName: project, ProjectNamespace: namespace, TopN: topN},
	})
	require.NoError(t, err)
	assert.NotEmpty(t, recommendations)
}

func TestRecommender_RecommendationsByMergeRequestCommit_Integration(t *testing.T) {
	var ok bool
	var envs map[string]string
	if envs, ok = testIsIntegrationTestRunnable(testEnvRecommenderBotUrl); !ok {
		t.Skip("skipping this integration test")
	}

	// Use the Gitaly fork project to run this integration test
	// URL: https://gitlab.com/gitlab-org/modelops/applied-ml/review-recommender/gitaly
	var projectId int64 = 31393306
	var commitSha = "7c34f0129b8947aa674f346830ed29272c217e3b"
	var project = "gitaly"
	var namespace = "gitlab-org/modelops/applied-ml/review-recommender"
	var state = "closed"
	var topN int32 = 3

	baseUrl := envs[testEnvRecommenderBotUrl]
	r, err := New(http.DefaultClient, baseUrl)
	require.NoError(t, err)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	// use the testing service that doesn't require authentication
	recommendations, err := r.RecommendationsByMergeRequestCommit(
		ctx, projectId, commitSha, state, domain.RecommenderServiceConfig{
			Model: domain.RecommenderModelConfig{ProjectName: project, ProjectNamespace: namespace, TopN: topN},
		},
	)
	require.NoError(t, err)
	assert.NotEmpty(t, recommendations)
}

func TestRecommender_PostMergeRequestRecommendations_Integration(t *testing.T) {
	var ok bool
	var envs map[string]string
	if envs, ok = testIsIntegrationTestRunnable(testEnvRecommenderBotUrl); !ok {
		t.Skip("skipping this integration test")
	}

	// Use the Gitaly fork project to run this integration test
	// URL: https://gitlab.com/gitlab-org/modelops/applied-ml/review-recommender/gitaly
	var projectId int64 = 31393306
	var iid int64 = 3

	baseUrl := envs[testEnvRecommenderBotUrl]
	r, err := New(http.DefaultClient, baseUrl)
	require.NoError(t, err)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	// use the testing service that doesn't require authentication
	err = r.PostMergeRequestRecommendations(
		ctx, domain.Recommendations{
			Iid:       iid,
			ProjectId: projectId,
			Changes:   []string{"README.md"},
			Reviewers: []string{"reviewer1"},
		},
		domain.RecommenderAuthConfig{},
	)
	require.NoError(t, err)
}
