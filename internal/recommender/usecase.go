package recommender

import (
	"context"

	"gitlab.com/gitlab-org/modelops/applied-ml/review-recommender/ci/internal/domain"
)

type recommenderUseCase struct {
	recommender domain.Recommender
	reporter    domain.Reporter
}

func (u *recommenderUseCase) MergeRequestRecommendations(
	ctx context.Context, projectId int64, identity domain.MergeRequestIdentity, config domain.RecommenderServiceConfig) (
	domain.Recommendations, error,
) {
	var err error
	var recommendations domain.Recommendations

	switch i := identity.(type) {
	case domain.MergeRequestByIid:
		recommendations, err = u.recommender.RecommendationsByMergeRequestIid(ctx, projectId, i.Iid, config)
	case domain.MergeRequestByCommit:
		recommendations, err = u.recommender.RecommendationsByMergeRequestCommit(ctx, projectId, i.Sha, i.State, config)
	default:
		err = domain.ErrMergeRequestIdentityNotFound
	}

	return recommendations, err
}

func (u *recommenderUseCase) PostMergeRequestRecommendations(
	ctx context.Context, recommendations domain.Recommendations, config domain.RecommenderAuthConfig,
) error {
	return u.recommender.PostMergeRequestRecommendations(ctx, recommendations, config)
}

func (u *recommenderUseCase) ReportMergeRequestRecommendations(
	ctx context.Context, recommendations domain.Recommendations, filename string,
) error {
	report := domain.Report{
		Meta: domain.ReportMeta{
			TopN:    recommendations.TopN,
			Version: recommendations.Version,
		},
		MergeRequest: domain.ReportMergeRequest{
			ProjectId:    recommendations.ProjectId,
			Iid:          recommendations.Iid,
			ChangedFiles: recommendations.Changes,
		},
		Recommendations: domain.ReportRecommendations{
			Reviewers: recommendations.Reviewers,
		},
	}
	err := u.reporter.Write(ctx, filename, report)
	if err != nil {
		return err
	}

	return nil
}

func NewUseCase(recommender domain.Recommender, reporter domain.Reporter) *recommenderUseCase {
	return &recommenderUseCase{
		recommender: recommender,
		reporter:    reporter,
	}
}
