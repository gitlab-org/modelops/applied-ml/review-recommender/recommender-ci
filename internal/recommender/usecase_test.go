package recommender

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/modelops/applied-ml/review-recommender/ci/internal/domain"
)

var (
	testExpectedRecommendations = domain.Recommendations{
		TopN:      3,
		Version:   "v0.1.0",
		Iid:       1,
		ProjectId: 995,
		Changes:   []string{"fileA"},
		Reviewers: []string{"reviewer1", "reviewer2", "reviewer3"},
	}
)

type testWrongIdentity struct {
	domain.MergeRequestIdentity
}

type testRecommenderMock struct {
	recommendationsByMergeRequestIid func(
		_ context.Context, _, _ int64, _ domain.RecommenderServiceConfig) (
		domain.Recommendations, error,
	)
	recommendationsByMergeRequestCommit func(
		_ context.Context, _ int64, _, _ string, _ domain.RecommenderServiceConfig) (
		domain.Recommendations, error,
	)
	postMergeRequestRecommendations func(
		_ context.Context, _ domain.Recommendations, _ domain.RecommenderAuthConfig) error
}

type testReporterMock struct {
	write func(_ context.Context, _ string, _ domain.Report) error
}

func (t *testRecommenderMock) RecommendationsByMergeRequestIid(
	ctx context.Context, projectId, iid int64, config domain.RecommenderServiceConfig) (
	domain.Recommendations, error,
) {
	return t.recommendationsByMergeRequestIid(ctx, projectId, iid, config)
}

func (t *testRecommenderMock) RecommendationsByMergeRequestCommit(
	ctx context.Context, projectId int64, sha, state string, config domain.RecommenderServiceConfig) (
	domain.Recommendations, error,
) {
	return t.recommendationsByMergeRequestCommit(ctx, projectId, sha, state, config)
}

func (t *testRecommenderMock) PostMergeRequestRecommendations(
	ctx context.Context, recommendations domain.Recommendations, config domain.RecommenderAuthConfig) error {
	return t.postMergeRequestRecommendations(ctx, recommendations, config)
}

func (t *testReporterMock) Write(
	ctx context.Context, filename string, report domain.Report) error {
	return t.write(ctx, filename, report)
}

// Unit tests

func TestRecommenderUseCase_MergeRequestRecommendations(t *testing.T) {
	expectedCommitSha := "abcd"
	errNotFound := errors.New("not found")

	recommender := new(testRecommenderMock)
	recommender.recommendationsByMergeRequestIid = func(
		_ context.Context, _, iid int64, _ domain.RecommenderServiceConfig) (
		domain.Recommendations, error,
	) {
		if iid != testExpectedRecommendations.Iid {
			return domain.Recommendations{}, errNotFound
		}
		return testExpectedRecommendations, nil
	}
	recommender.recommendationsByMergeRequestCommit = func(
		_ context.Context, _ int64, sha string, _ string, _ domain.RecommenderServiceConfig) (
		domain.Recommendations, error,
	) {
		if sha != expectedCommitSha {
			return domain.Recommendations{}, errNotFound
		}
		return testExpectedRecommendations, nil
	}

	tests := []struct {
		TestName      string
		ProjectId     int64
		Identity      domain.MergeRequestIdentity
		Config        domain.RecommenderServiceConfig
		Expected      domain.Recommendations
		ExpectedError error
	}{
		{
			TestName:  "found-by-id",
			ProjectId: testExpectedRecommendations.ProjectId,
			Identity:  domain.MergeRequestByIid{Iid: testExpectedRecommendations.Iid},
			Expected:  testExpectedRecommendations,
		},
		{
			TestName:      "iid-not-found",
			Identity:      domain.MergeRequestByIid{Iid: 0},
			ExpectedError: errNotFound,
		},
		{
			TestName:      "identity-not-found",
			Identity:      testWrongIdentity{},
			ExpectedError: domain.ErrMergeRequestIdentityNotFound,
		},
		{
			TestName:  "found-by-commit",
			ProjectId: testExpectedRecommendations.ProjectId,
			Identity:  domain.MergeRequestByCommit{Sha: expectedCommitSha, State: "opened"},
			Expected:  testExpectedRecommendations,
		},
		{
			TestName:      "commit-not-found",
			Identity:      domain.MergeRequestByCommit{State: "opened"},
			ExpectedError: errNotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.TestName, func(t *testing.T) {
			ucase := NewUseCase(recommender, nil)
			actual, err := ucase.MergeRequestRecommendations(context.TODO(), test.ProjectId, test.Identity, test.Config)
			assert.ErrorIs(t, err, test.ExpectedError)
			assert.Equal(t, test.Expected, actual)
		})
	}
}

func TestRecommenderUseCase_PostMergeRequestRecommendations(t *testing.T) {
	errNotFound := errors.New("not found")

	recommender := new(testRecommenderMock)
	recommender.postMergeRequestRecommendations =
		func(_ context.Context, rec domain.Recommendations, _ domain.RecommenderAuthConfig) error {
			if testExpectedRecommendations.Iid != rec.Iid {
				return errNotFound
			}
			return nil
		}

	tests := []struct {
		TestName        string
		Config          domain.RecommenderAuthConfig
		Recommendations domain.Recommendations
		ExpectedError   error
	}{
		{
			TestName:        "ok",
			Recommendations: testExpectedRecommendations,
		},
		{
			TestName:      "error",
			ExpectedError: errNotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.TestName, func(t *testing.T) {
			ucase := NewUseCase(recommender, nil)
			err := ucase.PostMergeRequestRecommendations(context.TODO(), test.Recommendations, test.Config)
			require.ErrorIs(t, err, test.ExpectedError)
		})
	}
}

func TestRecommenderUseCase_ReportMergeRequestRecommendations(t *testing.T) {
	errIncorrectFilename := errors.New("incorrect filename")
	reporter := new(testReporterMock)
	reporter.write = func(_ context.Context, filename string, _ domain.Report) error {
		if filename == "" {
			return errIncorrectFilename
		}
		return nil
	}

	tests := []struct {
		TestName        string
		Filename        string
		Recommendations domain.Recommendations
		ExpectedError   error
	}{
		{
			TestName:        "ok",
			Filename:        "report.txt",
			Recommendations: testExpectedRecommendations,
		},
		{
			TestName:      "error",
			ExpectedError: errIncorrectFilename,
		},
	}

	for _, test := range tests {
		t.Run(test.TestName, func(t *testing.T) {
			ucase := NewUseCase(nil, reporter)
			err := ucase.ReportMergeRequestRecommendations(context.TODO(), test.Recommendations, test.Filename)
			require.ErrorIs(t, err, test.ExpectedError)
		})
	}
}
