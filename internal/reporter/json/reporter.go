package jsonreporter

import (
	"context"
	"encoding/json"
	"os"
	"path/filepath"

	"gitlab.com/gitlab-org/modelops/applied-ml/review-recommender/ci/internal/domain"
)

const (
	defaultPermission = 0755
	jsonIndent        = "  "
)

type reporter struct{}

func (r *reporter) Write(_ context.Context, filename string, report domain.Report) error {
	if err := mkdirLocal(filename, defaultPermission); err != nil {
		return err
	}

	file, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, defaultPermission)
	if err != nil {
		return err
	}

	enc := json.NewEncoder(file)
	enc.SetIndent("", jsonIndent)

	err = enc.Encode(report)
	if err1 := file.Close(); err1 != nil && err == nil {
		err = err1
	}

	return err
}

func mkdirLocal(path string, perm os.FileMode) error {
	dir := filepath.Dir(path)
	return os.MkdirAll(dir, perm)
}

func New() *reporter {
	return &reporter{}
}
