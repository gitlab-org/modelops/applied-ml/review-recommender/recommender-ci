package jsonreporter

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/modelops/applied-ml/review-recommender/ci/internal/domain"
)

type testDirectory struct {
	Path string
}

func (d *testDirectory) InitTempDir() (err error) {
	tmpDir, err := ioutil.TempDir("", "json-reporter-*")
	d.Path = tmpDir

	return
}

func (d *testDirectory) CleanDir() {
	if err := os.RemoveAll(d.Path); err != nil {
		log.Warning(err)
	}

	d.Path = ""
}

var testDir = new(testDirectory)

var testReport = domain.Report{
	Meta: domain.ReportMeta{
		TopN:    3,
		Version: "v0.1.0",
	},
	MergeRequest: domain.ReportMergeRequest{
		ProjectId:    995,
		Iid:          1,
		ChangedFiles: []string{"README.md", "CHANGELOG", "main.go"},
	},
	Recommendations: domain.ReportRecommendations{
		Reviewers: []string{"approver1", "approver2", "approver3"},
	},
}

func TestMain(m *testing.M) {
	var exitCode int
	if err := testDir.InitTempDir(); err == nil {
		exitCode = m.Run()
		testDir.CleanDir()
	} else {
		exitCode = 1
		log.Error(err)
	}

	os.Exit(exitCode)
}

func TestReporter_Write(t *testing.T) {
	paths := []string{
		"report.json",
		"folder1/report.json",
		"folder1/folder2/report.json",
	}
	for _, path := range paths {
		t.Run(path, func(t *testing.T) {
			absPath := filepath.Join(testDir.Path, path)
			r := New()

			err := r.Write(context.TODO(), absPath, testReport)
			require.NoError(t, err)

			obtained, err := ioutil.ReadFile(absPath)
			require.NoError(t, err)

			var obtainedReport domain.Report
			err = json.Unmarshal(obtained, &obtainedReport)
			require.NoError(t, err)
			require.Equal(t, testReport, obtainedReport)
		})
	}
}
